import React from 'react';
import NavBarAdmin from '../components/NavBarAdmin';
import { styled } from '@mui/material/styles';

const Main = styled('main')(
    ({ theme }) => ({

    }),
);

function AdminLayout({ children }) {
    return (
        <div>
            <NavBarAdmin />
            <Main>
                {children}
            </Main>
        </div>
    )
}

export default AdminLayout
