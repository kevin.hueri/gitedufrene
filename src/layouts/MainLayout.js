import React from 'react'
import Footer from '../components/Footer';
import NavBar from '../components/NavBar';
import Fond from '../assets/photo07.jpg';
import { styled } from '@mui/material/styles';

const Main = styled('main')(
    ({ theme }) => ({
        backgroundImage: `url(${Fond})`,
        backgroundSize: 'cover',
        backgroundAttachment: 'fixed'
    }),
);

function MainLayout({ children }) {
    return (
        <>
            <NavBar />
            <Main>
                {children}
            </Main>
            <Footer />
        </>
    )
}

export default MainLayout
