import React, { useState, useCallback } from 'react';
import MainLayout from '../layouts/MainLayout';
import Titre from '../components/Titre';
import Gite from '../assets/gite/Le gite du Frene.jpg';
import Entree from '../assets/gite/Entree.jpeg';
import Cours from '../assets/gite/Cours.jpeg';
import GiteDeNuit from '../assets/gite/GiteDeNuit.jpeg';
import Salle01 from '../assets/salon/Salle001.jpeg';
import Salle02 from '../assets/salon/Salle008.jpeg';
import Salle03 from '../assets/salon/Salle015.jpeg';
import Salle04 from '../assets/salon/Salle018.jpeg';
import Entree1 from '../assets/entree/Palier_01.jpeg';
import Entree2 from '../assets/entree/Palier_02.jpeg';
import Entree3 from '../assets/entree/Entree_03.jpeg';
import Entree4 from '../assets/entree/Terrasse_02.jpeg';
import Cuisine1 from '../assets/cuisine/Cuisine001.jpeg';
import Cuisine2 from '../assets/cuisine/Cuisine002.jpeg';
import Chambre1 from '../assets/chambre/chambre1.jpeg';
import Chambre2 from '../assets/chambre/Chambre-ET-04.jpeg';
import Chambre3 from '../assets/chambre/Chambre-ET-05.jpeg';
import Chambre4 from '../assets/chambre/chambre-RDC-003 (2).jpeg';
import Barrage from '../assets/environnement/barrage.jpg';
import BaseNautique1 from '../assets/environnement/Base-nautique1.jpg';
import BaseNautique2 from '../assets/environnement/Base-nautique2.jpg';
import BaseNautique3 from '../assets/environnement/Base-nautique3.jpg';
import BioTropica1 from '../assets/environnement/Biotropica1.jpg';
import BioTropica2 from '../assets/environnement/Biotropica2.jpg';
import Golf1 from '../assets/environnement/golf1.jpg';
import Golf2 from '../assets/environnement/golf2.jpg';
import Golf3 from '../assets/environnement/golf3.jpg';
import Musee1 from '../assets/environnement/musee1.jpg';
import Musee2 from '../assets/environnement/musee2.jpg';
import Musee3 from '../assets/environnement/musee3.jpg';
import Promenade1 from '../assets/environnement/promenade1.jpg';
import Promenade2 from '../assets/environnement/promenade2.jpg';
import Promenade3 from '../assets/environnement/promenade3.jpg';
import Promenade4 from '../assets/environnement/promenade4.jpg';
import Village1 from '../assets/environnement/village1.jpg';
import Village2 from '../assets/environnement/village2.jpg';

import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";

function Gallerie() {
    const [currentImageGite, setCurrentImageGite] = useState(0);
    const [currentImageEnvironnement, setCurrentImageEnvironnement] = useState(0);
    const [viewerIsOpenGite, setViewerIsOpenGite] = useState(false);
    const [viewerIsOpenEnvironnement, setViewerIsOpenEnvironnement] = useState(false);

    const openLightboxGite = useCallback((event, { photo, index }) => {
        setCurrentImageGite(index);
        setCurrentImageEnvironnement(0);
        setViewerIsOpenGite(true);
        setViewerIsOpenEnvironnement(false);
    }, []);

    const openLightboxEnvironnement = useCallback((event, { photo, index }) => {
        setCurrentImageGite(0);
        setCurrentImageEnvironnement(index);
        setViewerIsOpenGite(false);
        setViewerIsOpenEnvironnement(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImageGite(0);
        setCurrentImageEnvironnement(0);
        setViewerIsOpenEnvironnement(false);
        setViewerIsOpenGite(false);
    };

    const imageListGite = [
        { src: Gite, width: 4, height: 3 },
        { src: Entree, width: 1, height: 1 },
        { src: Cours, width: 3, height: 4 },
        { src: GiteDeNuit, width: 3, height: 4 },
        { src: Salle01, width: 3, height: 4 },
        { src: Salle02, width: 4, height: 3 },
        { src: Salle03, width: 3, height: 4 },
        { src: Salle04, width: 4, height: 3 },
        { src: Entree1, width: 4, height: 3 },
        { src: Entree2, width: 4, height: 3 },
        { src: Entree3, width: 1, height: 1 },
        { src: Entree4, width: 3, height: 4 },
        { src: Cuisine1, width: 3, height: 4 },
        { src: Cuisine2, width: 3, height: 4 },
        { src: Chambre1, width: 4, height: 3 },
        { src: Chambre2, width: 3, height: 4 },
        { src: Chambre3, width: 4, height: 3 },
        { src: Chambre4, width: 4, height: 3 }
    ]

    const imageListEnvironnement = [
        { src: Barrage, width: 4, height: 3 },
        { src: BaseNautique1, width: 1, height: 1 },
        { src: BaseNautique2, width: 3, height: 4 },
        { src: BaseNautique3, width: 3, height: 4 },
        { src: BioTropica1, width: 3, height: 4 },
        { src: BioTropica2, width: 4, height: 3 },
        { src: Golf1, width: 3, height: 4 },
        { src: Golf2, width: 4, height: 3 },
        { src: Golf3, width: 4, height: 3 },
        { src: Musee1, width: 4, height: 3 },
        { src: Musee2, width: 1, height: 1 },
        { src: Musee3, width: 3, height: 4 },
        { src: Promenade1, width: 3, height: 4 },
        { src: Promenade2, width: 3, height: 4 },
        { src: Promenade3, width: 4, height: 3 },
        { src: Promenade4, width: 3, height: 4 },
        { src: Village1, width: 4, height: 3 },
        { src: Village2, width: 4, height: 3 }
    ]

    return (
        <MainLayout>
            <Titre />

            {/* Le gite */}
            <div className='gite'>
                <h2 className='giteTitle'>L'hebergement</h2>
                <Gallery photos={imageListGite} onClick={openLightboxGite} />
                <ModalGateway>
                    {viewerIsOpenGite ? (
                        <Modal onClose={closeLightbox}>
                            <Carousel
                                currentIndex={currentImageGite}
                                views={imageListGite.map(x => ({
                                    ...x,
                                    srcset: x.srcSet,
                                    caption: x.title
                                }))}
                            />
                        </Modal>
                    ) : null}
                </ModalGateway>
            </div>

            {/* L'environnement'*/}
            <div className='gite'>
                <h2 className='giteTitle'>L'environnement</h2>
                <Gallery photos={imageListEnvironnement} onClick={openLightboxEnvironnement} />
                <ModalGateway>
                    {viewerIsOpenEnvironnement ? (
                        <Modal onClose={closeLightbox}>
                            <Carousel
                                currentIndex={currentImageEnvironnement}
                                views={imageListEnvironnement.map(x => ({
                                    ...x,
                                    srcset: x.srcSet,
                                    caption: x.title
                                }))}
                            />
                        </Modal>
                    ) : null}
                </ModalGateway>
            </div>
        </MainLayout>
    )
}

export default Gallerie