import React, { useState } from 'react';
import MainLayout from '../layouts/MainLayout';
import Titre from '../components/Titre';
import Table from 'react-bootstrap/Table';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

function TarifsDispo() {
    const [value, onChange] = useState(new Date());

    return (
        <MainLayout>
            <Titre />

            {/* Tarifs */}
            <div className='situation'>
                <h2 className='situationTitle'>Tarifs</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Durée</th>
                            <th>Prix</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Semaine (7 nuits)</td>
                            <td>750 euros - remise 15%= 637,50 euros</td>
                        </tr>
                        <tr>
                            <td>Mid week (4 nuits)</td>
                            <td>360 euros</td>
                        </tr>
                        <tr>
                            <td>Week end (2 nuits)</td>
                            <td>260euros</td>
                        </tr>
                    </tbody>
                </Table>
                <p className='taxeSejour'>
                    Taxe de séjour: 0,75 euro par personne et par nuit. <br />
                    Merci de nous contacter pour toutes demandes particulieres
                </p>
            </div>

            {/* Logement */}
            <div className='logement'>
                <h2 className='logementTitle'>Calendrier</h2>
                <Calendar onChange={onChange} value={value} />
            </div>
        </MainLayout>
    )
}

export default TarifsDispo