import React from 'react';
import MainLayout from '../layouts/MainLayout';
import Titre from '../components/Titre';
import Card from 'react-bootstrap/Card';

function LivreDOr() {
    const livreList = [
        { titre: 'Bruno', subtitre: 'Janvier 2022', text: 'Séjour très agréable' },
        { titre: 'Charlotte', subtitre: 'Décembre 2021', text: 'This is a lovely place to stay, with everything you need and more under one roof. The hosts were extremly friendly and welcoming! We came in the winter month and the hosts made sure we were all warm and the heating was set for us. 4 adults shared the house and it was spacious enough. Very well looked after and clean, with space for parking our vehicle too.' },
        { titre: 'Nathalie', subtitre: 'Novembre 2021', text: 'Marie Christine et Philippe sont l\'hôte et l\'hôtesse parfaits. Nous avons apprécié notre séjour. Un logement agréable, soigné et idéal à faire avec des enfants.' },
        { titre: 'Bernard', subtitre: 'Novembre 2021', text: 'Nous reviendrons dans ce gite lumineux et confortable , avec un bon accueil et bien situé!' },
        { titre: 'David', subtitre: 'Novembre 2021', text: 'top, qualité du logement et prestations' },
        { titre: 'Anonyme', subtitre: 'Octobre 2020', text: 'Gite cocooning. Le cadre est jolie a coter d une base nautique. La maison a du charme et la propreter est impeccable. Les propritaires sont accueillant et tres gentil et tres discret.' },
        { titre: 'Jörg Grondke', subtitre: 'Octobre 2020', text: 'Emplacement parfait. Très bel appartement dans un quartier calme. Tout était parfait.' },
        { titre: 'Eric', subtitre: 'Octobre 2021', text: 'Très bon accueil. je remercie Marie Christine et Philippe pour leur accueil dans leur gîte très bien placé à Pose surtout pour les activités aquatique. merci ' },
        { titre: 'Marléne', subtitre: 'Octobre 2021', text: 'Accueil extrêmement attentionné. Grande propreté des lieux, gîte conforme aux photographies, lieu agréable, lumineux et très bien équipé.' },
        { titre: 'Ségoléne', subtitre: 'Octobre 2021', text: 'Très belle expérience dans le gîte de Marie Christine et Philippe, qui nous ont très bien accueillis et qui avaient à cœur de nous faire apprécier leur région avec plein de bons conseils. Le gîte est fidèle aux photos, très propre et cocooning. Il est très bien situé et permet de varier les plaisirs en pleine nature, la Seine passant a proximité ou visites avec Rouen à 20 min. Nos hôtes ont su jongler entre discrétion et présence pour rendre notre séjour le plus plaisant et satisfaisant. Nous recommandons ce gîte .' },
        { titre: 'Sandie', subtitre: 'Décembre 2020', text: 'A recommander absolument ! Un accueil généreux de la part de Marie-Christine et Philippe, le gîte lui même est spacieux et lumineux, très bien équipé et parfaitement adapté aux familles (jeux pour enfants, barrière de sécurité dans les escaliers), très propre et très agréable à vivre. Bref ce séjour fut parfait ! Merci' },
        { titre: 'Franck', subtitre: 'Juin 2020', text: 'Marie Christine et Phillippe son super, accueillant, au petit soin, avec pleins de petites touche attention, des hôtes qui plaisir à faire ce qu\'il font et ça ce vois! La maison et tout simplement conforme aux photos, magnifique, design épuré, tout équipé et confortable... un havre de paix avec un jardin calme et très bien entretenu ! Que dire de plus si ce n\'est merci!!! Franck' }
    ]

    return (
        <MainLayout>
            <Titre />

            {/* Livre d'or */}
            <div className='Livre'>
                <h2 className='situationTitle'>Livre d'or</h2>
                {livreList.map((item, index) => (
                    <Card key={index} className='cardLivreDOr'>
                        <Card.Body>
                            <div className='cardHaut'>
                                <Card.Title className='cardTitle'>{item.titre}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted cardSubtitle">{item.subtitre}</Card.Subtitle>
                            </div>
                            <Card.Text className='cardText'>
                                {item.text}
                            </Card.Text>
                        </Card.Body>
                    </Card>
                ))}
            </div>
        </MainLayout>
    )
}

export default LivreDOr