import React from 'react';
import MainLayout from '../layouts/MainLayout';
import Titre from '../components/Titre';

function Home() {
    return (
        <MainLayout>
            <Titre />

            {/* Situation */}
            <div className='situation'>
                <h2 className='situationTitle'>Situation</h2>
                <p>
                    Charmant village en bord de Seine, très calme, 1 200 habitants. Chemin du Halage avec vue sur la colline des deux Amants, Barrage et écluses. Musée de la Batellerie. <br />
                    Epicerie, dépôt de pain à 600 mètres du gîte. <br />
                    Tous commerces et marché (dimanche matin) à Pont de l’Arche (8 kms) <br />
                    Commerces et marché (vendredi après-midi) à Val de Reuil (5.6 kms) <br />
                    Gare SNCF à Val de Reuil (5 kms). <br />
                    Entrée et/ou sortie d’autoroute à 10 kms et à 12 kms.
                </p>
            </div>

            {/* Logement */}
            <div className='logement'>
                <h2 className='logementTitle'>Le logement</h2>
                <p>
                    Le gîte se situe à Poses, village de mariniers, entre la Seine et les étangs de la base nautique. <br />
                    Maison en bois située dans une voie sans issue, sur le même terrain que le propriétaire, dont une partie sur l’arrière de leur maison est privatisée. <br />
                    Le logement comprend :
                </p>
                <ul className='listLogement'>
                    <li className='cardLogement1'>
                        <p>Rez de chaussée</p>
                        <ul className='listServices'>
                            <li><p>Entrée</p></li>
                            <li><p>Placard de rangement</p></li>
                            <li><p>Chambre 2 lits de 80 x 200 pouvant faire un lit de 160</p></li>
                            <li><p>Salle de bain attenante avec baignoire et toilette</p></li>
                            <li><p>Escalier en colimaçon menant à l'étage</p></li>
                        </ul>
                    </li>
                    <li className='cardLogement2'>
                        <p>A l'étage en mansarde</p>
                        <ul className='listServices'>
                            <li><p>Cuisine ouverte avec four</p></li>
                            <li><p>Plaque vitrocéramique, micro-ondes, lave vaisselle, cafetière Tassimo, bouilloire, grille pain</p></li>
                            <li><p>Séjour salon avec canapé, TV écran plat</p></li>
                            <li><p>Une salle d'eau avec grande douche et wc</p></li>
                            <li><p>Une grande chambre mansardée avec placard, lit 160</p></li>
                        </ul>
                    </li>
                    <li className='cardLogement3'>
                        <p>Services</p>
                        <ul className='listServices'>
                            <li><p>Wifi gratuite dans le logement</p></li>
                            <li><p>Terrasse exterieure avec pergola et jacuzzi (ouverture debut mai a fin fermeture fin septembre)</p></li>
                            <li><p>Une place de parking devant le gîte, portail électrique. </p></li>
                        </ul>
                    </li>
                </ul>
            </div>

            {/* Acces voyageur */}
            <div className='acces'>
                <h2 className='accesTitle'>Accès</h2>
                <p>
                    Vous aurez accès a l'intégralité de la maison sauf le local technique et les 2 garages du bas. <br />
                    1 place de parking devant le gîte, portail électrique <br />
                    Vélos à disposition gratuitement et remorque enfants (sous conditions) <br />
                    Le gîte se situe sur le grand jardin de la propriété de Marie Christine et Philippe dont une partie sur l'arrière leur est privatisée.
                </p>
            </div>

        </MainLayout>
    )
}

export default Home