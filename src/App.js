import React from "react";
import { HashRouter, Routes, Route } from 'react-router-dom';
import Home from "./pages/Home";
import LivreDOr from "./pages/LivreDOr";
import Gallerie from "./pages/Gallerie";
import TarifsDispo from "./pages/TarifsDispo";
import Admin from "./pages/Admin";


const App = () => {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/livre-d-or" exact element={<LivreDOr />} />
        <Route path="/gallerie-d-images" exact element={<Gallerie />} />
        <Route path="/tarifs-et-disponibilites" exact element={<TarifsDispo />} />

        <Route path="/admin" exact element={<Admin />} />
      </Routes>
    </HashRouter>
  );
}


export default App;
