import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLocationDot } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

function Footer() {
    return (
        <div className='footer'>
            {/* Adresse Desktop */}
            <div className='adresse'>
                <div className='location'>
                    <FontAwesomeIcon className='locationIcon' icon={faLocationDot} />
                    <div className='locationText'>
                        <h5>Adresse</h5>
                        <p>
                            Gite du Frêne <br />
                            4 rue du port <br />
                            27740 Poses
                        </p>
                    </div>
                </div>

                <div className='phone'>
                    <FontAwesomeIcon className='phoneIcon' icon={faPhone} />
                    <div className='phoneText'>
                        <h5>Téléphone</h5>
                        <p>
                            06 42 84 02 69
                        </p>
                    </div>
                </div>

                <div className='mail'>
                    <FontAwesomeIcon className='mailIcon' icon={faEnvelope} />
                    <div className='mailText'>
                        <h5>EMail</h5>
                        <p>
                            legitedufrene@orange.fr
                        </p>
                    </div>
                </div>
            </div>

            {/* Links */}
            <div className='links'>
                <a href="#">Mentions légales</a>
            </div>

            {/* Copyright */}
            <div className='copyright'>
                <p>Copyright@2022 Gite du Frêne</p>
            </div>
        </div>
    )
}

export default Footer
