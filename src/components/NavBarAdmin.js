import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faScrewdriverWrench } from "@fortawesome/free-solid-svg-icons";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import { faImage } from "@fortawesome/free-solid-svg-icons";
import { faTag } from "@fortawesome/free-solid-svg-icons";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

const Link = (props) => {
    const { item, active, setActive } = props;
    const [classMenuActive, setClassMenuActive] = useState('menuListAdminItemDiv');

    useEffect(
        () => {
            if (active === item.id) {
                setClassMenuActive('menuListAdminItemDiv active')
            } else if (active !== item.id) {
                setClassMenuActive('menuListAdminItemDiv')
            }
        }, [active]);

    return (
        <a href={item.link} onClick={() => setActive(item.id)}>
            <li className='menuListAdminItem'>
                <div className={classMenuActive}>
                    <div className='menuListAdminItemIcon'>
                        <FontAwesomeIcon className='adminButtonIcon' icon={item.icon} />
                    </div>
                    <div className='menuListAdminItemText'>
                        <span>{item.titre}</span>
                    </div>
                </div>
            </li>
        </a>
    )
}

function NavBarAdmin() {

    const [active, setActive] = useState(null);
    const [activeMenu, setActiveMenu] = useState('active');

    const handleCloseMenu = () => {
        setActiveMenu('disactive')
    }
    const adminMenuList = [
        { id: 1, titre: 'Home', link: '#/admin', icon: faHouse },
        { id: 2, titre: 'Gallerie d\'images', link: '#/admin', icon: faImage },
        { id: 3, titre: 'Tarifs & disponibilités', link: '#/admin', icon: faTag },
        { id: 4, titre: 'Livres d\'or', link: '#/admin', icon: faBook },
        { id: 5, titre: 'Déconnexion', link: '#/admin', icon: faRightFromBracket },
    ]

    return (
        <div className='navBarAdmin' id={activeMenu}>
            <div className='adminTitre'>
                <FontAwesomeIcon className='adminCloseIcon' icon={faXmark} onClick={() => handleCloseMenu()} />
                <a className='adminButton' href='#'>
                    <FontAwesomeIcon className='adminButtonIcon' icon={faScrewdriverWrench} />
                    <h2 className='adminButtonText'>administrateur</h2>
                </a>
            </div>
            <hr className='hrAdmin' />
            <ul className='menuListAdmin'>
                {adminMenuList.map((item, index) => (
                    <Link key={index} item={item} active={active} setActive={setActive} />
                ))}
            </ul>
        </div>
    )
}

export default NavBarAdmin