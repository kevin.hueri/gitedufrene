import React, { useState, useEffect } from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from "@fortawesome/free-solid-svg-icons";

import { useNavigate } from 'react-router-dom';

function NavBar() {
    const navigate = useNavigate();
    const [isActive, setIsActive] = useState(false);
    const [scrollTop, setScrollTop] = useState(0);

    const onScroll = (e) => { setScrollTop(e.target.documentElement.scrollTop) }
    useEffect(() => { window.addEventListener('scroll', onScroll) }, []);
    useEffect(() => { if (scrollTop > 50) { setIsActive(true) } else { setIsActive(false) } }, [scrollTop])
    let classNameMenu = "menu";
    if (isActive) { classNameMenu = 'menu affix' } else if (!isActive) { classNameMenu = 'menu' }
    return (
        <>
            <Navbar bg="dark" variant="dark" className={classNameMenu}>
                <Container className='navBar'>
                    <Navbar.Brand className='homeNavbar' onClick={() => navigate({ pathname: '/' })}>
                        <FontAwesomeIcon className='home' icon={faHome} />
                    </Navbar.Brand>
                    <Nav className="navLink">
                        <Nav.Link onClick={() => navigate({ pathname: '/gallerie-d-images' })}>Gallerie d'images</Nav.Link>
                        <Nav.Link onClick={() => navigate({ pathname: '/tarifs-et-disponibilites' })}>Tarifs & disponibilités</Nav.Link>
                        <Nav.Link onClick={() => navigate({ pathname: '/livre-d-or' })}>Livre d'or</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    )
}

export default NavBar
