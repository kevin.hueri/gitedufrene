import React from 'react'
import Navbar from '../components/NavBar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from "@fortawesome/free-solid-svg-icons";

function Titre() {
    return (
        <>
            <Navbar />

            {/* Titre */}
            <div className='title'>
                <div className='stars'>
                    <FontAwesomeIcon className='star' icon={faStar} />
                    <FontAwesomeIcon className='star' icon={faStar} />
                    <FontAwesomeIcon className='star' icon={faStar} />
                </div>
                <div className='titrePrincipal'>
                    <h1>
                        Gite du Frêne <br />
                        Poses
                    </h1>
                </div>
                <div className='stars'>
                    <FontAwesomeIcon className='star' icon={faStar} />
                    <FontAwesomeIcon className='star' icon={faStar} />
                    <FontAwesomeIcon className='star' icon={faStar} />
                </div>
            </div>
        </>
    )
}

export default Titre
